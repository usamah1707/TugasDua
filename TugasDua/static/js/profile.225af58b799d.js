
    // Setup an event listener to make an API call once auth is complete
	function onLinkedInLoad() {
		IN.Event.on(IN, "auth", getProfileData);
	}
	
    // Use the API call wrapper to request the user's basic profile data
    function getProfileData() {
		IN.API.Profile("me").fields("id", "firstName", "last-name", "headline", "location", "picture-urls::(original)", "public-profile-url", "email-address").result(displayProfileData).error(onError);
	}
	
	function displayProfileData(data) {
		var user = data.values[0];
		console.log(user);
        #document.getElementById("picture").innerHTML = '<img src="'+user.pictureUrls.values[0]+'" />';
        document.getElementById("name").innerHTML = user.firstName+' '+user.lastName;
        document.getElementById("intro").innerHTML = user.headline;
        document.getElementById("email").innerHTML = user.emailAddress;
        document.getElementById("location").innerHTML = user.location.name;
        document.getElementById("link").innerHTML = '<a href="'+user.publicProfileUrl+'" target="_blank">Visit profile</a>';
        document.getElementById('profileData').style.display = 'block';
    //use information captured above
	}

    // Handle the successful return from the API call
    function onSuccess(data) {
        console.log(data);
    }

    // Handle an error response from the API call
    function onError(error) {
        console.log(error);
    }
	
	function logout(){
        IN.User.logout(removeProfileData);
    }
    
    // Remove profile data from page
    function removeProfileData(){
        document.getElementById('profileData').remove();
    }
	
	var firstName;