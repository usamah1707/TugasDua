
    // Setup an event listener to make an API call once auth is complete
	function onLinkedInLoad() {
		IN.Event.on(IN, "auth", getProfileData);
	}
	
    // Use the API call wrapper to request the user's basic profile data
    function getProfileData() {
		IN.API.Profile("me").fields("id", "firstName", "last-name", "headline", "location", "picture-urls::(original)", "public-profile-url", "email-address").result(displayProfileData).error(onError);
	}
	
	function displayProfileData(data) {
		var user = data.values[0];
		console.log(user);
        //document.getElementById("picture").innerHTML = '<img src="'+user.pictureUrls.values[0]+'" />';
        //document.getElementById("name").innerHTML = user.firstName+' '+user.lastName;
        //document.getElementById("intro").innerHTML = user.headline;
        //document.getElementById("email").innerHTML = user.emailAddress;
        //document.getElementById("location").innerHTML = user.location.name;
        //document.getElementById("link").innerHTML = '<a href="'+user.publicProfileUrl+'" target="_blank">Visit profile</a>';
        //document.getElementById('profileData').style.display = 'block';
		fname = user.firstName;
		lname = user.lastName;
		email = user.emailAddress;
		ppu = user.publicProfileUrl;
		var html = '<p><button style="btn btn-warning" href="javascript:void(0);" onclick="logout()">Logout</button></p>' + 
						//picture
				'<div><p id="name">'+ fname+' '+lname +'</p>'+
				'<p id="intro"> '+ user.headline + '</p>' +
				'<p id="email">' + email +'</p>' + 
				'<p id="location">'+ user.location.name +'</p>' +
				'<p id="link"><a href="'+user.publicProfileUrl +'" target="_blank">Visit profile</a></p></div>';
		$("#profileData").append(html);
    //use information captured above
	}

    // Handle the successful return from the API call
    function onSuccess(data) {
        console.log(data);
    }

    // Handle an error response from the API call
    function onError(error) {
        console.log(error);
    }
	
	function logout(){
        IN.User.logout(removeProfileData);
    }
    
    // Remove profile data from page
    function removeProfileData(){
        document.getElementById('profileData').remove();
    }
	
	function simpanData(){
		var keahlian = $("#keahlian").var;
		var level = $("#level").var;
		addToModel("{{npm}}", fname+' '+lname, email, ppu, keahlian, level);
	}
	
	function addToModel(npm, name, email, linkedin, keahlian, level){
        $.ajax({
            method: "POST",
            data: {npm:npm, name: name, email: email, linkedin: linkedin, keahlian:keahlian, level:level, csrfmiddlewaretoken: '{% csrf_token %}'},
			url: '{% url "index:addToModel" %}',
            success : function (d) {
                //console.log("SUKSES");
            },
			
            error : function (error) {
				//console.log("GAGAL");
            }
        });
    }
	
	var fname, lname, email, ppu;