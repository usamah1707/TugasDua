"""TugasDua URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.conf.urls import url, include
from django.contrib import admin
import halaman_profile.urls as profile
import cari_mahasiswa.urls as cari_mahasiswa
import halamanLogin.urls as halamanLogin
import halamanStatus.urls as halamanStatus
import halamanRiwayat.urls as halamanRiwayat

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^', include(halamanLogin,namespace='halamanLogin')),
    url(r'^teman/', include(cari_mahasiswa,namespace='teman')),
    url(r'^profile/', include(profile, namespace='profile')),
    url(r'^halamanStatus', include(halamanStatus,namespace='halamanStatus')),
    url(r'^halamanRiwayat/', include(halamanRiwayat,namespace="halamanRiwayat")),
]
