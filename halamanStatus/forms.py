from django import forms

class Form_status(forms.Form):
    error_messages = {
        'required': 'Tolong isi input ini',
    }

    status_attrs = {
        'type': 'text',
        'cols': 45,
        'rows': 5,
        'class': 'todo-form-textarea',
        'placeholder':'whats going on?'
    }

    status = forms.CharField(label='', required=True, widget=forms.Textarea(attrs=status_attrs))
