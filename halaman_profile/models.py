from django.db import models

# Create your models here.
class UserProfile(models.Model):
    username = models.CharField(max_length=20,default="Kosong")
    name = models.CharField(max_length=100,default="Kosong")
    npm = models.CharField(max_length=15,default="Kosong")
    keahlian = models.CharField(max_length=250,default="Kosong")
    level = models.CharField(max_length=250,default="Kosong")
    email = models.TextField(default="Kosong")
    linkedin = models.CharField(max_length=250,default="Kosong")
    added_at = models.DateField(auto_now_add=True)
	
    def as_dict(self):
            return {
                "username": self.username,
                "name": self.npm,
                "npm": self.npm,
                "keahlian": self.keahlian,
                "email": self.email,
                "linkedin": self.linkedin,
            }
