
    // Setup an event listener to make an API call once auth is complete
	function onLinkedInLoad() {
		IN.Event.on(IN, "auth", getProfileData);
	}
	
    // Use the API call wrapper to request the user's basic profile data
    function getProfileData() {
		IN.API.Profile("me").fields("id", "firstName", "last-name", "headline", "location", "picture-urls::(original)", "public-profile-url", "email-address").result(displayProfileData).error(onError);
	}
	
	function displayProfileData(data) {
		var user = data.values[0];
		console.log(user);
        //document.getElementById("picture").innerHTML = '<img src="'+user.pictureUrls.values[0]+'" />';
		fname = user.firstName;
		lname = user.lastName;
		email = user.emailAddress;
		ppu = user.publicProfileUrl;
		var html =  '<button class="btn btn-danger" href="javascript:void(0);" onclick="logout()">Bukan Data Saya?</button>' +
						//picture
				'<hr>'+
				'<div><p id="name">NAMA : '+ fname+' '+lname +'</p>'+
				'<p id="email">EMAIL : ' + email +'</p>' + 
				'<p id="location">LOKASI :'+ user.location.name +'</p>' +
				'<p id="link"><a href="'+user.publicProfileUrl +'" target="_blank">Visit profile</a></p></div>';
		$("#profileData").append(html);
    //use information captured above
	}

    // Handle the successful return from the API call
    function onSuccess(data) {
        console.log(data);
    }

    // Handle an error response from the API call
    function onError(error) {
        console.log(error);
    }
	
	function logout(){
        IN.User.logout(removeProfileData);
    }
    
    // Remove profile data from page
    function removeProfileData(){
        document.getElementById('profileData').remove();
    }
	
	function simpanData(){
		var keahlian = $(".keahlian").val();
		var level = $(".level").val();
		addToModel('{{npm}}', fname+' '+lname, email, ppu, keahlian, level);
	}
	
	function addToModel(npm, name, email, linkedin, keahlian, level){
        $.ajax({
            method: "POST",
			url: '../addtomodel/',
            data: {npm:npm, name: name, email: email, linkedin: linkedin, keahlian: keahlian, level:level, csrfmiddlewaretoken: '{{ csrf_token }}'},
            success : function (d) {
                //console.log("SUKSES");
				alert("Informasi Sudah terupdate");
            },
			
            error : function (error) {
				//console.log("GAGAL");
            }
        });
    }
	
	var fname, lname, email, ppu;