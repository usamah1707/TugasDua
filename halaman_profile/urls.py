from django.conf.urls import url
from .views import index, edit, addToModel

urlpatterns = [
    url(r'^$', index, name='index'),
	url(r'^edit/', edit, name='edit'),
	url(r'^addtomodel/', addToModel, name='addtomodel'),
]
