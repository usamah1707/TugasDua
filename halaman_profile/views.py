from django.shortcuts import render
from django.http import HttpResponse, HttpResponseRedirect, JsonResponse, HttpResponseNotFound
from .models import UserProfile
from django.views.decorators.csrf import csrf_exempt
from halamanLogin.models import User

# Create your views here.
response={}
def index(request):
    if get_data_user(request, 'user_login'):
        set_data_for_session(request)
        npm = get_data_user(request, 'kode_identitas')
        if(not UserProfile.objects.filter(npm=npm).exists()):
            UserProfile(npm=npm).save()
			
        user = UserProfile.objects.filter(npm=npm).get()
        
        response["user"] = user
        response["npm"] = npm
        html = "halaman_profile/profile.html"
        return render(request, html, response)
    else:
        response['login'] = False
        html = 'session/home.html'
        return render(request, html, response)

		
@csrf_exempt
def addToModel(request):
    npm = response['npm']
    name = request.POST['name']
    email = request.POST['email']
    linkedin = request.POST['linkedin']
    keahlian = request.POST['keahlian']
    level = request.POST['level']
 
    UserProfile.objects.filter(npm=npm).update(name=name, email=email, linkedin=linkedin, keahlian=keahlian, level=level)
	
    return HttpResponseRedirect('/profile/edit')
def edit(request):
    response={}
    html = "halaman_profile/edit.html"
    return render(request, html, response)
	
def set_data_for_session(request):
	response['author'] = get_data_user(request, 'user_login')
	response['kode_identitas'] = request.session['kode_identitas']
	response['role'] = request.session['role']
	
def get_data_user(request, tipe):
	data = None
	if tipe == "user_login" and 'user_login' in request.session:
		data = request.session['user_login']
	elif tipe == "kode_identitas" and 'kode_identitas' in request.session:
		data = request.session['kode_identitas']
	elif tipe == "role" and 'role' in request.session:
		data = request.session['role']
	return data

	
